# Version 3.3.2

Version 3.3.2 was released on 8 Aug 2021 NZST

This is a hotfix - if your game already works fine, upgrading to this version won't change anything.

* Fix crashes on computers with all.blb or any other files (not folders) with names shorter than eight letters in the assets directory - ZNix
* Fix a compile issue on Linux - roberChen

# Version 3.3.1 and below

Version 3.3.1 was released on 7 Jun 2021 NZST

These versions do not have nicely formatted changelogs, but you can see their Git commit history in GitLab if required.
